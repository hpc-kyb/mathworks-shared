
Command line just with singularity:

module load singularity

singularity run /ptmp/containers/matlab_r2020b-2021-05-10.sif -nosplash -nodesktop -nojvm -r "run('/home/bfitch/mathworks-shared/cpu_bench.m'); exit;"

