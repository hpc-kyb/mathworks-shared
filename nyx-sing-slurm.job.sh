#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J ml_bench
#
#
# Queue (Partition):
#SBATCH --partition=compute  # nyx partitions: compute, highmem, gpu
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --tasks=1
#SBATCH --cpus-per-task=64
#SBATCH --exclusive
#
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=blake.fitch@tuebingen.mpg.de
#
# Wall clock limit:
#SBATCH --time=01:00:00

module purge
module load singularity

export SINGULARITY_BIND="/ptmp,/scratch"
export SINGULARITYENV_MATLABPATH=$PWD
export MATLAB_CONTAINER_PATH=/ptmp/containers/matlab_r2020b-2021-05-10.sif
#export MATLAB_CONTAINER_PATH=/ptmp/containers/matlab_r2021a-2021-05-10.sif

echo "Here we go..."
time srun ${MATLAB_CONTAINER_PATH} -nosplash -nodesktop -nojvm -r "run('/home/bfitch/mathworks-shared/cpu_bench.m'); exit;"
echo " ... done."


